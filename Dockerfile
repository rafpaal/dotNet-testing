FROM microsoft/dotnet
WORKDIR /app
COPY ./ ./
RUN ["dotnet", "restore"]
ENV ASPNETCORE_URLS http://+:80
CMD ["dotnet", "run"]

